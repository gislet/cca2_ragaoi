﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerObject : MonoBehaviour {

	/* */
	public Text hud_name;
	public Text hud_size;

	/* */
	private float _current_size;
	private float _current_radius;
	private bool _is_touch_device = false;


	/* */
	public void updateVisuals(PlayerInfo p_info) {
		_current_size = p_info.getSize();
		_current_radius = p_info.getRadius();
		transform.localScale = Vector3.one * (_current_radius * 2 * Global.SCALE_NORMALIZER);
		hud_name.text = p_info.name;
		hud_size.text = p_info.size.ToString("f1");
	}


	/* */
	public void run() {
		Vector3 tracker_positon = Vector3.zero;
		if(Input.touchCount > 0) {
			_is_touch_device = true;
			tracker_positon = new Vector3(Camera.main.ScreenToViewportPoint(Input.GetTouch(0).position).x - .5f, Camera.main.ScreenToViewportPoint(Input.GetTouch(0).position).y - .5f, 0).normalized;
		} else if(!_is_touch_device) {
			if(Vector3.Distance(Vector3.zero, new Vector3(Camera.main.ScreenToViewportPoint(Input.mousePosition).x - .5f, Camera.main.ScreenToViewportPoint(Input.mousePosition).y - .5f, 0)) > .05f)
				tracker_positon = new Vector3(Camera.main.ScreenToViewportPoint(Input.mousePosition).x - .5f, Camera.main.ScreenToViewportPoint(Input.mousePosition).y - .5f, 0).normalized;
		}
		Vector3 new_position = transform.position + tracker_positon * Time.deltaTime * speed;

		//int x = Input.GetKey(KeyCode.D) ? 1 : Input.GetKey(KeyCode.A) ? -1 : 0;
		//int y = Input.GetKey(KeyCode.W) ? 1 : Input.GetKey(KeyCode.S) ? -1 : 0;

		//Vector3 new_position = new Vector3(transform.position.x + (x * Time.deltaTime * speed), transform.position.y + (y * Time.deltaTime * speed));
		validateNewPosition(ref new_position);
		transform.position = new_position;
	}


	/* */
	private void validateNewPosition(ref Vector3 new_position) {
		if(WorldHandler.getWorldSize().x == -1)
			return;

		float x = new_position.x;
		float y = new_position.y;

		new_position.x = x + _current_radius >= WorldHandler.getWorldSize().x / 2 ? WorldHandler.getWorldSize().x / 2 - _current_radius :
			x - _current_radius <= -(WorldHandler.getWorldSize().x / 2) ? -(WorldHandler.getWorldSize().x / 2) + _current_radius :
			x;

		new_position.y = y + _current_radius >= WorldHandler.getWorldSize().y / 2 ? WorldHandler.getWorldSize().y / 2 - _current_radius :
			y - _current_radius <= -(WorldHandler.getWorldSize().y / 2) ? -(WorldHandler.getWorldSize().y / 2) + _current_radius :
			y;
	}


	/* */
	private float speed {
		get {
			float s = Global.BASE_SPEED;

			s -= (_current_radius - 1);

			return s >= Global.MIN_SPEED ? s : Global.MIN_SPEED;
		}
	}


	/* */
	public float radius {
		get {
			return _current_radius;
		}
	}

	public float size {
		get {
			return _current_size;
		}
	}

}
