﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public static class Network {

	/* */
	private static WebSocket _socket;
	private static readonly string SERVER_ADDR = "18.221.150.186";
	private static readonly string SERVER_PORT = "11113";

	/* */
	private static NetworkStatus _status;



	/* */
	static Network() {
		_status = NetworkStatus.Offline;
	}


	/* */
	public static void connect() {
		Master.mono.StartCoroutine(openConnection());
	}


	/* */
	public static void disconnect(String p_error = null) {
		if(_socket == null)
			return;

		if(p_error != null)
			Debug.LogError("Disconnected due to error: " + p_error);

		_status = NetworkStatus.Disconnected;
		_socket.Close();
		_socket = null;
	}


	/* */
	private static IEnumerator openConnection() {

		//Create the socket and attempt connection.
		_socket = new WebSocket(new Uri("ws://" + SERVER_ADDR + ":" + SERVER_PORT));
		yield return Master.mono.StartCoroutine(_socket.Connect());

		if(_socket.error == null) {
			connectionSuccessful();			
		} else {
			Debug.LogError("Failed to connect: " + _socket.error);
		}

		//
		while(_status == NetworkStatus.Connected) {

			string reply = _socket.RecvString();
			if(reply != null) {
				processReply(reply);

				if(Master.player.isAlive) {
					sendUpdates();
				}
			}			

			yield return null;
		}

		disconnect();
	}


	/* Connection was successful. We're sending our initial JSON with our player info. */
	private static void connectionSuccessful() {
		_status = NetworkStatus.Connected;
		_socket.SendString(Json.toString(new PlayerInfo(Master.getPlayerName)));
	}


	/* */
	private static void sendUpdates() {
		_socket.SendString(Json.toString(Master.player.info));
	}


	/* */
	private static void processReply(String p_msg) {

		if(!Master.player.isAlive && Master.player.obj == null) {
			PlayerInfo PI = Json.toObject<PlayerInfo>(p_msg);
			if(PI != null) {
				//Debug.Log("PI: " + p_msg);
				Master.player.setup(PI);
			}
			return;
		}

		GameStatus GS = Json.toObject<GameStatus>(p_msg);
		if(GS != null) {
			WorldHandler.setWorld(GS);
			EnemyHandler.update(GS.players);
			FoodHandler.update(GS.food);
		}
	}


	public static NetworkStatus status { get { return _status; } }
}
