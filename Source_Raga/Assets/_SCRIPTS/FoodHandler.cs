﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodHandler {




	public FoodHandler() {
		_food = new List<FoodContainer>();
		//Master.mono.StartCoroutine(update());
	}


	/* */
	public static void update(FoodInfo[] p_food) {
		if(p_food == null || _food == null)
			return;

		List<int> player_indices = new List<int>();
		for(int i = 0; i < _food.Count; i++) {
			player_indices.Add(i);
		}

		for(int i = 0; i < p_food.Length; i++) {
			bool exists = false;

			for(int g = 0; g < _food.Count; g++) {
				if(p_food[i].ID == _food[g].info.ID) {
					player_indices.Remove(g);
					exists = true;
					break;
				}
			}

			if(!exists) {
				FoodContainer FC = new FoodContainer();
				FC.info = p_food[i];
				FC.obj = Master.instantiate(Master.getFoodPrefab, FC.info.getPosition()) as FoodObject;
				FC.obj.setupVisuals(FC.info);
				_food.Add(FC);
			}
		}

		//Removing any players who aren't playing anymore.
		for(int i = player_indices.Count - 1; i >= 0; i--) {
			FoodContainer FC = _food[player_indices[i]];
			Master.destroy(FC.obj.gameObject);

			_food.RemoveAt(player_indices[i]);
		}
	}

	public class FoodContainer {
		public FoodObject obj;
		public FoodInfo info;
	}

	private static List<FoodContainer> _food { get; set; }
}
