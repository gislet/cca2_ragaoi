﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class GameStatus {
	public int gameworld_x;
	public int gameworld_y;

	public PlayerInfo[] players;
	public FoodInfo[] food;
}
