﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Global {

	public static readonly float SCALE_NORMALIZER = 5f;
	public static readonly float WORLD_PADDING = 10f;
	public static readonly float BASE_SPEED = 3.5f;
	public static readonly float MIN_SPEED = 2;
}
