﻿/****************************************************************************** 
 *
 * JsonHandler.cs
 *
 * <Description>
 *
 *
 * Written by:
 *	- Gisle Thorsen
 *
 * Dated: September 2017
 *
 *****************************************************************************/


using LitJson;
using System;
using System.Collections.Generic;
using UnityEngine;

public static class Json {

	/* */
	public static string toString<T>(T p_object, bool p_pretty_print = false) {
		JsonWriter writer = new JsonWriter();
		writer.PrettyPrint = p_pretty_print;
		JsonMapper.ToJson(p_object, writer);
		return writer.ToString();
	}


	/*public static string serializeList<T>(List<T> p_list, bool p_pretty_print = false) {
		JsonWriter writer = new JsonWriter();
		writer.PrettyPrint = p_pretty_print;
		JsonMapper.ToJson(p_list, writer);
		return writer.ToString();
	}*/


	
	public static T toObject<T>(string p_data) where T : class {
		try {
			JsonReader reader = new JsonReader(p_data);
			return JsonMapper.ToObject<T>(reader);
		} catch(Exception e) {
			Debug.Log("" + e);
			return null;
		}
	}


	
	public static List<T> toObjectList<T>(string p_data) where T : class {
		try {
			JsonReader reader = new JsonReader(p_data);
			return JsonMapper.ToObject<List<T>>(reader);
		} catch(Exception e) {
			return null;
		}
	}

}
