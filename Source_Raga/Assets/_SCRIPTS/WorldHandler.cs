﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldHandler : MonoBehaviour {

	/* */
	[SerializeField]
	private GameObject _background;
	[SerializeField]
	private SpriteRenderer _border;

	/* */
	private static WorldHandler _instance;

	/* */
	private int _world_size_x = -1;
	private int _world_size_y = -1;
	private readonly float BORDER_SCALE = 5;



	/* */
	private void Awake() {
		_instance = this;
	}


	/* */
	public static void setWorld(GameStatus p_gs) {
		if(p_gs.gameworld_x == _instance._world_size_x && _instance._world_size_y == p_gs.gameworld_y) {
			return;
		}

		_instance._world_size_x = p_gs.gameworld_x;
		_instance._world_size_y = p_gs.gameworld_y;

		_instance._background.transform.localScale = new Vector3(p_gs.gameworld_x * 2 + Global.WORLD_PADDING, p_gs.gameworld_y * 2 + Global.WORLD_PADDING, 1);
		_instance._border.size = new Vector2(p_gs.gameworld_x / _instance.BORDER_SCALE, p_gs.gameworld_y / _instance.BORDER_SCALE);
	}


	public static Vector2 getWorldSize() {
		return new Vector2(_instance._world_size_x, _instance._world_size_y);
	}
}
