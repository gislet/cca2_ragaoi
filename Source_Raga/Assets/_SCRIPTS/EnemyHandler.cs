﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHandler {

	/* */
	//private List<PlayerContainer> _enemy_players;


	/* */
	private bool _coroutine_alive;


	public EnemyHandler() {
		_enemy_players = new List<PlayerContainer>();
		Master.mono.StartCoroutine(update());
	}


	public static void reset() {
		_enemy_players = new List<PlayerContainer>();
	}


	/* */
	public static void update(PlayerInfo[] p_enemies) {
		if(p_enemies == null || _enemy_players == null)
			return;

		List<int> player_indices = new List<int>();
		for(int i = 0; i < _enemy_players.Count; i++) {
			player_indices.Add(i);
		}

		for(int i = 0; i < p_enemies.Length; i++) {
			bool exists = false;

			//No point in processing this PI if it's the player.
			if(p_enemies[i].ID == Master.player.ID) {
				Master.player.obj.updateVisuals(p_enemies[i]);
				continue;
			}

			for(int g = 0; g < _enemy_players.Count; g++) {
				if(p_enemies[i].ID == _enemy_players[g].info.ID) {
					update(p_enemies[i], g);
					player_indices.Remove(g);
					exists = true;
					break;
				}
			}

			if(!exists) {
				PlayerContainer PC = new PlayerContainer();
				PC.info = p_enemies[i];
				PC.obj = Master.instantiate(Master.getPlayerPrefab, PC.info.getPosition()) as PlayerObject;
				PC.obj.updateVisuals(PC.info);
				_enemy_players.Add(PC);
			}
		}

		//Removing any players who aren't playing anymore.
		for(int i = player_indices.Count - 1; i >= 0; i--) {
			PlayerContainer PC = _enemy_players[player_indices[i]];
			Master.destroy(PC.obj.gameObject);

			_enemy_players.RemoveAt(player_indices[i]);
		}
	}


	/* */
	private static void update(PlayerInfo p_info, int p_index) {
		PlayerContainer PC = _enemy_players[p_index];
		PC.info = p_info;
		PC.obj.updateVisuals(PC.info);

		//TODO update any other important changes. (like name change)
	}


	/* */
	private IEnumerator update() {

		while(true) {
			//TODO maybe add some kind of code to process server disconnection? seeing as this is a static class.

			for(int i = 0; i < _enemy_players.Count; i++) {
				PlayerContainer PC = _enemy_players[i];
				PC.obj.transform.position = Vector2.Lerp(PC.obj.transform.position, PC.info.getPosition(), Time.deltaTime * 5);
			}

			yield return null;
		}
	}


	public class PlayerContainer {
		public PlayerObject obj;
		public PlayerInfo info;
	}


	private static List<PlayerContainer> _enemy_players { get; set; }
}
