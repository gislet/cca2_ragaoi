﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodInfo {

	/* */
	public string ID;

	/* */
	public double x;
	public double y;

	/* */
	public double size;


	public FoodInfo() { }


	/* */
	public FoodInfo(FoodInfo p_image) {
		ID = p_image.ID;
		setSize((float)p_image.size);
		setPosition(p_image.getPosition());
	}


	/* */
	public void setSize(float p_size) {
		size = p_size;
	}


	public float getSize() {
		return (float)size;
	}


	public float getRadius() {
		double radi_sqrd = size / Math.PI;
		return (float)Math.Sqrt(radi_sqrd);
	}


	public void setPosition(Vector2 p_position) {
		x = p_position.x;
		y = p_position.y;
	}


	public Vector2 getPosition() {
		return new Vector2((float)x, (float)y);
	}
}
