﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Master : MonoBehaviour {

	private static Master _instance;
	private Player _player;
	private EnemyHandler _enemy_handler;
	private FoodHandler _food_handler;
	public string player_name;
	public PlayerObject player_prefab;
	public FoodObject food_prefab;

	public InputField name_field;
	public GameObject menu_UI, gameover_UI, quit_UI;

	private void Start() {
		//Vital! MUST INIT FIRST
		_instance = this;

		_enemy_handler = new EnemyHandler();
		_food_handler = new FoodHandler();
	}


	public void play() {
		if(name_field.text.Length == 0 || name_field.text.Length > 10)
			return;

		player_name = name_field.text;
		menu_UI.SetActive(false);

		Network.connect();
	}

	public void leave_game() {
		gameover_UI.SetActive(false);
		quit_UI.SetActive(false);
		menu_UI.SetActive(true);
		Network.disconnect();
		player.destroy();
		_player = null;
	}


	public void resume() {
		quit_UI.SetActive(false);
	}


	public static void quit_menu() {
		_instance.quit_UI.SetActive(true);
	}


	private void Update() {
		switch(Network.status) {
		case NetworkStatus.Connecting:
			break;
		case NetworkStatus.Connected:
			if(player.obj != null && !player.isAlive)
				gameover_UI.SetActive(true);
			break;
		case NetworkStatus.Disconnected:
			break;
		}
	}


	/* */
	private void OnApplicationQuit() {
		Network.disconnect();
	}


	/* */
	public static Object instantiate(Object p_object, Vector2 p_position) {
		return Instantiate(p_object, p_position, Quaternion.identity);
	}


	/* */
	public static void destroy(GameObject p_object) {
		Destroy(p_object);
	}


	#region Getters & Setters
	public static MonoBehaviour mono { get { return _instance; } }
	public static string getPlayerName { get { return _instance.player_name; } }
	public static PlayerObject getPlayerPrefab { get { return _instance.player_prefab; } }
	public static FoodObject getFoodPrefab { get { return _instance.food_prefab; } }
	public static Player player {
		get {
			if(_instance._player == null)
				_instance._player = new Player();
			return _instance._player;
		}
	}
	public static bool isQuitMenuActive {
		get {
			return _instance.quit_UI.activeSelf;
		}
	}
	#endregion
}
