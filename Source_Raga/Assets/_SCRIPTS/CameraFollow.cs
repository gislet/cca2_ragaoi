﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {	
	
	void Update () {
		if(Master.player.isAlive) {
			float z = 10 * (Master.player.obj.radius * (5/3f));
			z = z < 10 ? 10 : z;

			Vector3 player_pos = Master.player.obj.transform.position;
			player_pos.z = -z;
			transform.position = Vector3.Lerp(transform.position, player_pos, Time.deltaTime * 5);
		}
	}
}
