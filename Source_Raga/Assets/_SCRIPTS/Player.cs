﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player {

	/* */
	private string _name;
	private string _ID;

	/* */
	private PlayerObject _player_object;
	private bool _is_alive;

	/* */
	private bool _coroutine_alive;


	public Player() {
		_is_alive = false;
		_coroutine_alive = true;
		Master.mono.StartCoroutine(update());
	}


	/* */
	public void setup(PlayerInfo p_info) {
		_name = p_info.name;
		_ID = p_info.ID;

		if(_player_object == null) {
			instantiatePlayer(p_info);
		}
	}


	/* */
	private IEnumerator update() {
		while(_coroutine_alive) {
			if(_is_alive && !Master.isQuitMenuActive) {
				_is_alive = _player_object.size > .1f;
				_player_object.run();

				if(Input.GetMouseButton(1) || Input.GetKeyDown(KeyCode.Escape))
					Master.quit_menu();
			}
			yield return null;
		}
	}


	/* */
	public void destroy() {
		_is_alive = false;
		_ID = null;
		_name = null;
		Master.destroy(_player_object.gameObject);
	}


	/* */
	private void instantiatePlayer(PlayerInfo p_info) {
		_player_object = Master.instantiate(Master.getPlayerPrefab, p_info.getPosition()) as PlayerObject;
		_player_object.updateVisuals(p_info);
		_is_alive = true;
	}


	#region Getters & 
	public string name { get { return _name; } }
	public string ID { get { return _ID; } }
	public bool isAlive { get { return _is_alive; } }
	public PlayerObject obj { get { return _player_object; } }
	public PlayerInfo info {
		get {
			PlayerInfo info = new PlayerInfo(name);
			info.ID = ID;

			info.setPosition(_player_object.transform.position);

			return info;
		}
	}
	#endregion

}
