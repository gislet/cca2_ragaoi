﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodObject : MonoBehaviour {

	/* */
	public void setupVisuals(FoodInfo p_info) {
		transform.localScale = Vector3.one * (p_info.getRadius() * 2 * Global.SCALE_NORMALIZER);
	}
}
