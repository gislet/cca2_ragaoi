﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NetworkStatus {
	Offline,
	Connecting,
	Connected,
	Disconnected
}
