﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class EchoTest : MonoBehaviour {

	public Text t;
	public Text clock;

	WebSocket w;
	IEnumerator Start() {
		//Application.runInBackground = true;
		StartCoroutine(timer());

		w = new WebSocket(new Uri("ws://18.221.150.186:11113"));
		yield return StartCoroutine(w.Connect());
		t.text = "Connected";
		w.SendString("Good day, from the browser");
		t.text = "First Msg Sent";
		int i = 0;
		while(true) {
			string reply = w.RecvString();
			if(reply != null) {
				t.text = reply;
				//Debug.Log("Received: " + reply);
				w.SendString("update " + i++);
			}
			if(w.error != null) {
				t.text = w.error;
				Debug.LogError("Error: " + w.error);
				break;
			}
			yield return 0;
		}
		t.text = "Disconnected";
		w.Close();
	}


	private IEnumerator timer() {		
		while(true) {
			clock.text = "" + Time.time;
			yield return null;
		}
	}

	private void OnApplicationQuit() {
		if(w != null)
			w.Close();
	}
}